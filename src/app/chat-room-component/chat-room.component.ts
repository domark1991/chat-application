import { Component, TemplateRef, ViewChild } from '@angular/core';
import { MessagingService } from '../services/messaging.service';
import { MessageModel } from '../models/message.model';
import { ChatParticipant } from '../models/chatparticipant.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
    selector: 'app-chat-room',
    templateUrl: './chat-room.component.html',
    styleUrls: ['./chat-room.component.css']
})
export class ChatRoomComponent {
    index = 1;
    private chatRoomParticipants: ChatParticipant[] = [];

    addParticipant(template: any) {
        if (this.chatRoomParticipants.length < 10) {
            let chatParticipant = new ChatParticipant();
            chatParticipant.id = '' + this.index;
            chatParticipant.name = 'Chat Box ' + this.index;
            this.chatRoomParticipants.push(chatParticipant);
            this.index++;
        } else {
            this.openModal(template)
        }
    }

    closeChat(event: any) {
        let index = this.chatRoomParticipants.indexOf(event)
        this.chatRoomParticipants.splice(index, 1);
    }

    modalRef: BsModalRef;
    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }
}
