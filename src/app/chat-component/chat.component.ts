import { Component, AfterContentInit, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { MessagingService } from '../services/messaging.service';
import { MessageModel } from '../models/message.model';

import { ChatParticipant } from '../models/chatparticipant.model';
import { Observable } from 'rxjs';
import { timer } from 'rxjs/observable/timer';
import { of } from 'rxjs/observable/of';
import { delay } from 'rxjs/operators';

@Component({
    selector: 'app-chat-window',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.css']
})
export class ChatComponent implements AfterViewInit {
    @Input() chatParticipant: ChatParticipant;
    @Output() closeChat: EventEmitter<ChatParticipant> = new EventEmitter();
    messageInput: any;
    messageLog: MessageModel[];
    whoIsTyping: ChatParticipant;
    timer: Observable<any>;
    userHasStartedTyping = false;

    constructor(private messagingService: MessagingService) {
        this.messageLog = [];
    }

    closeButtonClicked() {
        this.closeChat.emit(this.chatParticipant);
    }

    sendButtonClicked() {
        if (this.messageInput && this.messageInput.trim() !== '') {
            let messageModel = new MessageModel();
            messageModel.message = this.messageInput;
            messageModel.sendername = this.chatParticipant.name;
            messageModel.senderid = this.chatParticipant.id;
            this.messagingService.messageServer.emit(messageModel);
            this.messageInput = '';
        }
    }

    ngAfterViewInit() {
        this.messagingService.messageServer.subscribe(data => {
            if (data) {
                this.messageLog.push(data);
            }
        })

        this.messagingService.whoIsTyping.subscribe(data => {
            this.whoIsTyping = data;
        })
    }

    onValueChanged() {
        if (!this.userHasStartedTyping) {
            this.userHasStartedTyping = true;
            if (this.messageInput && this.messageInput.trim() !== '') {
                this.messagingService.whoIsTyping.emit(this.chatParticipant);
                of('Typing Complete').pipe(delay(2000)).subscribe(data => {
                    this.messagingService.whoIsTyping.emit(null);
                    this.userHasStartedTyping = false;
                });
            }
        }
    }
}
