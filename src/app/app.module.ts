import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ChatComponent } from './chat-component/chat.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MessagingService } from './services/messaging.service';
import { ChatRoomComponent } from './chat-room-component/chat-room.component';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    ChatRoomComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ModalModule.forRoot()
  ],
  providers: [MessagingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
