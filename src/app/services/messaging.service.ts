import { Injectable, EventEmitter } from '@angular/core';
import { MessageModel } from '../models/message.model';
import { ChatParticipant } from '../models/chatparticipant.model';

@Injectable()
export class MessagingService {
    messageServer: EventEmitter<MessageModel>;
    whoIsTyping: EventEmitter<ChatParticipant>;

    constructor() {
        this.messageServer = new EventEmitter();
        this.whoIsTyping = new EventEmitter();
    }
}